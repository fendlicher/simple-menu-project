/**
 * PANCAKE FLIPPER ALGORITHM
 * 
 * @param {number[]} A input pancake row (coding: "happy side" = '1', "blank side" = '0')
 * @param {number} K consecutive pancakes to flip
 * @return {number} minimum number of times you will need to use the oversized pancake flipper / -1 if IMPOSSIBLE
 */
var minKBitFlips = function(A, K) {
    let r = 0;
    for(let i = 0; i<A.length; i++) {
	    // if we come across 0 we  want to alter it (so we alter also K-1 following values 
        if(A[i] === 0 && i<A.length - K + 1) {
            for(let j = i; j< i + K; j++) {
                // boolean invert, then cast back to a Number
                A[j] = +!A[j]; 
            }
            r++;
        }
	    // we check if we are left with any 0. If so, then there is no valid combination and we can return 0
        if(A[i] === 0) return 'IMPOSSIBLE';
    }
    return r;
};

// read input pancake rows from command line
var myArgs = process.argv.slice(2);

let S = [];
let K = [];
// push inputs into arrays and convert +/- to 1/0 for minKBitFlips algorithm
for (let i = 1; i < myArgs.length; i++) {
    if (i%2 === 0) {
        K.push(myArgs[i]);
    }
    else {
        S.push(myArgs[i].split('').map(x => (x === '+' ? 1 : 0)));
    }
}
for (let i = 0; i < S.length; i++) {
    process.stdout.write('Case #' +i+ ': ');
    process.stdout.write(minKBitFlips(S[i], parseInt(K[i])) + ' ');
}
process.stdout.write('\n');




