import React, { useState, useEffect } from 'react';

import './App.css';
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from 'reactstrap';

import { getMenuData } from './api';
import { MenuLink } from './stories/MenuLink';

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const [iFrameSrc, setiFrameSrc] = useState('');
  const [menuData, setMenuData] = useState(getMenuData());
  useEffect(() => {
    // api call N/A - replace mock data 
    fetch("https://www.we-conect.com/api/v1/menu")
        .then(response => {
          setMenuData(response.json());
        })
    updateIframe(window.location.pathname.substring(1));

  }, [window.location.pathname]);
  const toggle = () => setIsOpen(!isOpen);
  const renderMenu = (data) => {
    let menuItems = [];
    for (let i = 0; i < data.length; i++) {
      const menuItem = data[i];
      if (menuItem.children) {
        let subItems = [];
        for (let index = 0; index < menuItem.children.length; index++) {
          const subItem = menuItem.children[index];
          subItems.push(
            <MenuLink
              key={`subitemlink-${i}-${index}`}
              label={subItem.title}
              link={subItem.slug}
            ></MenuLink>
          );
        }
        menuItems.push(
          <UncontrolledDropdown nav inNavbar key={`menuitemlink-${i}`}>
            <DropdownToggle nav caret>
              {menuItem.title}
            </DropdownToggle>
            <DropdownMenu right>{subItems}</DropdownMenu>
          </UncontrolledDropdown>
        );
      } else {
        menuItems.push(
          <MenuLink
            key={`menuitemlink-${i}`}
            label={menuItem.title}
            link={menuItem.slug}
          ></MenuLink>
        );
      }
    }
    return menuItems;
  };
  const updateIframe = (name) => {
    let itemSrc = '';
    for (let a = 0; a < menuData.length; a++) {
      const item = menuData[a];
      if (item.slug === name) {
        itemSrc = item.url;
      }
      if (item.children) {
        for (let b = 0; b < item.children.length; b++) {
          const subItem = item.children[b];
          if (subItem.slug === name) {
            itemSrc = subItem.url;
          }
        }
      }
    }
    setiFrameSrc(itemSrc);
  };
  return (
    <div
      className="App"
      style={{
        height: '100vh',
      }}
    >
      <Navbar color="dark" light>
        <Navbar
          style={{
            width: '100%',
          }}
          color="light"
          light
          expand="md"
        >
          <NavbarBrand href="/">Simple Challenge</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              {renderMenu(menuData)}
            </Nav>
          </Collapse>
        </Navbar>
      </Navbar>
      <Container
        style={{
          height: '100vh',
        }}
        fluid={true}
      >
        <iframe
          src={iFrameSrc}
          style={{
            width: '100%',
            height: '90vh',
            minWidth: '100%',
          }}
        />
      </Container>
    </div>
  );
}

export default App;
