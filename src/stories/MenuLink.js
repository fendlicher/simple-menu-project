import React from 'react';
import PropTypes from 'prop-types';

import {
  NavItem,
  NavLink,

} from 'reactstrap';
/**
 * Primary UI component for user interaction
 */
export const MenuLink = ({ link, label, onClick }) => {
  return (
    <NavItem>
      <NavLink href={link} onClick={onClick}>
        {label}
      </NavLink>
    </NavItem>
  );
};

MenuLink.propTypes = {
  /**
   * MenuLink contents
   */
  label: PropTypes.string,
  /**
   * Optional click handler
   */
  link: PropTypes.string,
  onClick: PropTypes.func
};

MenuLink.defaultProps = {

};
