import React from 'react';

import { MenuLink } from './MenuLink';

export default {
  title: 'Example/MenuLink',
  component: MenuLink,
};

const Template = (args) => <MenuLink {...args} />;

export const Simple = Template.bind({});
Simple.args = {
  label: 'MenuItem',
  link: 'http://test.de',
};
