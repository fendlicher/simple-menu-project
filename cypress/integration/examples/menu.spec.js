// plugin to test storybook
import 'cypress-storybook/react'

context('Main page', () => {
  it('main page visit successful', () => {
    cy.visit('/');
  });
});

context('Menu links', () => {
  it('menu links operational', () => {
    cy.visit('/')
    cy.get('ul>li').eq(0).click();
    cy.get('div>li').eq(0).click();
    cy.location('pathname').should('include', '/live');

    cy.get('ul>li').eq(0).click();
    cy.get('div>li').eq(1).click();
    cy.location('pathname').should('include', '/digital');

    cy.get('ul>li').eq(1).click();
    cy.location('pathname').should('include', '/google');
  })
})
